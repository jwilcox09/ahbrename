# Rename

Renaming your app in Rails 3 is no longer a hassle.
This plugin allows you to rename your Ruby on Rails 3 app seamlessly with a single command.

## Installation

	% gem 'rename', git: 'git@bitbucket.org:jwilcox09/ahbrename.git'

## Usage

Manually rename your main project directory to SweetNewName and run:

	% rails g rename_to SweetNewName

That will rename your app in the following files:

	config/application.rb
	config/environment.rb
	config/environments/development.rb
	config/environments/test.rb
	config/environments/production.rb
	config/routes.rb
	config.ru
	config/initializers/secret_token.rb
	config/initializers/session_store.rb
	config/deploy.rb
	config/unicorn.rb
	config/nginx.rb
	config/unicorn_init.sh
	.ruby-gemset
