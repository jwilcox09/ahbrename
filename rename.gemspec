# -*- encoding: utf-8 -*-
require File.expand_path('../lib/rename/version', __FILE__)

Gem::Specification.new do |gem|
  gem.name          = 'rename'
  gem.version       = Rename::VERSION
  gem.license       = 'MIT'
  gem.authors       = ['Joshua Wilcox']
  gem.email         = %w(me@joshuawilcox.net)
  gem.homepage      = 'https://bitbucket.org/jwilcox09/ahbrename'
  gem.description   = 'This library allows you to rename rails application using a single command'
  gem.summary       = 'A library to rename your rails3 application'

  gem.add_dependency 'rails','>= 3.0.0'
  gem.rubyforge_project = 'rename'

  gem.files         = `git ls-files`.split("\n")
  gem.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  gem.require_paths = %w(lib)
end